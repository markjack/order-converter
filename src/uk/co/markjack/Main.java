package uk.co.markjack;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class Main {

    private static File orderFile;
    private static String fileName, fileExtension, folderPath, customerID, orderingSystem, customerPONumber, wantedDeliveryDate, amzAddressCode;
    private static String[][] customers, orderLines, amazonAccountsArray, amazonAddressesArray;
    private static String[] fileNames;
    private static boolean useSPCR, empFifthColumn, treatAsItemsMoreThan99, freshOrPantry, amazonSups;
    private static int aniOrderLinesReadCorrectly;
    private static int aniOrderLinesError;
    private static int wfmOrderLinesReadCorrectly;
    private static int wfmOrderLinesError;
    private static int empOrderLinesReadCorrectly;
    private static int empOrderLinesError;
    private static int natOrderLinesReadCorrectly;
    private static int natOrderLinesError;
    private static int xmlFilesCreated;
    private static int amazonOrdersExtracted;
    private static int lineToProcessEmpOrderFrom;
    private static final String AS_NATURE_INTENDED_ORDER = "ANI";
    private static final String ANI_PO_IDENTIFIER = "Order";
    private static final String ANI_PO_IDENTIFIER_EXCLUSION = "Order Date";
    private static final String ANI_ORDER_DATE_IDENTIFIER = "Delivery Date";
    private static final String ANI_LINES_IDENTIFIER = "Number of Lines";
    private static final String ANI_TABLE_START_IDENTIFIER = "Supplier Product Code";
    private static final String WHOLE_FOODS_ORDER = "WFM";
    private static final String WFM_PO_IDENTIFIER = "Order Number:";
    private static final String WFM_ORDER_DATE_IDENTIFIER = "Expected Delivery Date:";
    private static final String WFM_TOTAL_ITEMS_IDENTIFIER = "Totals:";
    private static final String WFM_TABLE_START_IDENTIFIER = "Item No.";
    private static final String WFM_COLUMN_SEPARATOR = "</td>";
    private static final String WFM_QTY_TEXT_FOR_REMOVAL = "&nbsp;&nbsp;EA";
    private static final String EMPORIO_ORDER = "Emp";
    private static final String EMP_ORDERLINES_START_IDENTIFIER = ">Description<";
    private static final String EMP_ORDERLINES_END_IDENTIFIER = ">Total<";
    private static final String AMAZON_ORDER = "Amz";
    private static final String AMAZON_ORDER_IDENTIFIER = "PO,Vendor,Warehouse";
    private static final String AMAZON_PANTRY = "PUKLY";
    private static final String AMAZON_FRESH = "PUKLZ";
    private static final String AMAZON_SUPS = "501682";
    private static final String SPCR_USED = "SPCR-yes";
    private static final String FIFTH_COLUMN = "fifth";
    private static final String NATURISIMO_ORDER = "Nat";
    private static final String NAT_PO_IDENTIFIER = "PO No.";
    private static final String NAT_ORDER_DATE_IDENTIFIER = "Order Date";
    private static final String NAT_TOTAL_ITEMS_IDENTIFIER = "SUB TOTAL";
    private static final String NAT_TABLE_START_IDENTIFIER = "MOQ<br>Stock";

    // TODO delete some of the system messages
    public static void main(String[] args) {
        int wfmOrdersFound = 0;
        int amazonOrderFileFound = 0;
        int aniOrdersFound = 0;
        int empOrdersFound = 0;
        int natOrdersFound = 0;
        aniOrderLinesReadCorrectly = aniOrderLinesError = wfmOrderLinesReadCorrectly
                = wfmOrderLinesError = empOrderLinesReadCorrectly = empOrderLinesError
                = xmlFilesCreated = amazonOrdersExtracted = natOrderLinesError = natOrderLinesReadCorrectly = 0;
        getFileNames();
        readCustomers();
        if (fileNames != null) {
            int count = 0;
            int numberOfFiles = fileNames.length;
            System.out.println("Number of files: " + numberOfFiles);
            while (count < numberOfFiles) {
                //get name of file for turning into XML later
                String[] parts = fileNames[count].split("\\.");
                fileName = parts[0];
                if (parts.length>1) fileExtension = parts[1];
                else fileExtension = null;
                orderFile = new File(folderPath + "\\" + fileNames[count]);
                orderCustomer();
                if (orderingSystem != null) {
                    switch (orderingSystem) {
                        case AMAZON_ORDER:
                            amazonOrderFileFound++;
                            // Amazon orders will be handled differently to the others as they contain multiple order in one file
                            processAmazonOrder();
                            break;
                        case AS_NATURE_INTENDED_ORDER:
                            aniOrdersFound++;
                            extractANIOrder();
                            if (orderLines!=null) createXML();
                            else infoBox("No lines read for an ANI order", "Error!");
                            break;
                        case WHOLE_FOODS_ORDER:
                            wfmOrdersFound++;
                            extractWFMOrder();
                            if (orderLines!=null) createXML();
                            else infoBox("No lines read for a WFM order", "Error!");
                            break;
                        case NATURISIMO_ORDER:
                            natOrdersFound++;
                            extractNatOrder();
                            if (orderLines!=null) createXML();
                            else infoBox("No lines read for a Naturisimo order", "Error!");
                            break;
                        case EMPORIO_ORDER:
                            empOrdersFound++;
                            extractEmpOrder();
                            if (orderLines!=null) createXML();
                            else infoBox("No lines read for an Emporio order", "Error!");
                            break;
                        default:
                            System.out.println("no customer found");
                            break;
                    }
                } else {
                    System.out.println("no customer found");
                }
                count++;
                clearFields();
            }
            String aniStatement = "";
            String wfmStatement = "";
            String amzStatement = "";
            String empStatement = "";
            String natStatement = "";
            if (aniOrdersFound > 0)
                aniStatement = aniOrdersFound + " ANI orders found & processed, " + aniOrderLinesReadCorrectly + " passed checks & "
                        + aniOrderLinesError + " failed checks.\n";
            if (wfmOrdersFound > 0) wfmStatement = wfmOrdersFound + " WFM orders found & processed, "
                    + wfmOrderLinesReadCorrectly + " passed checks & " + wfmOrderLinesError + " failed checks.\n";
            if (empOrdersFound > 0) empStatement = empOrdersFound + " Emporio orders found & processed, "
                    + empOrderLinesReadCorrectly + " passed checks & " + empOrderLinesError + " failed checks.\n";
            if (amazonOrderFileFound > 0)
                amzStatement = amazonOrderFileFound + " Amazon files found & " + amazonOrdersExtracted + " orders extracted.\n";
            if (natOrdersFound > 0) natStatement = natOrdersFound + " Naturisimo orders found & processed, "
                    + natOrderLinesReadCorrectly + " passed checks & " + natOrderLinesError + " failed checks.\n";
            infoBox(aniStatement + wfmStatement + empStatement + amzStatement + natStatement
                    + xmlFilesCreated + " XML files written to " + folderPath, null);
        } else {
            infoBox("a problem occurred finding files", null);
        }
    }

    private static void readCustomers() {
        try {
            File file = new File(folderPath + "\\customers.txt");
            if (file.exists() && !file.isDirectory()) {
                FileReader fileReader = new FileReader(folderPath + "\\customers.txt");
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line;
                List<String[]> customerList = new ArrayList<>();
                List<String[]> amazonAccounts = new ArrayList<>();
                List<String[]> amazonAddresses = new ArrayList<>();
                while ((line = bufferedReader.readLine()) != null) {
                    if (line.startsWith("//")) {
                        continue;
                    }
                    String[] str = line.split(",");
                    if (str.length == 6) customerList.add(str);
                    if (str.length == 4) amazonAccounts.add(str);
                    if (str.length == 2) amazonAddresses.add(str);
                }
                fileReader.close();
                customers = customerList.toArray(new String[0][0]);
                amazonAccountsArray = amazonAccounts.toArray(new String[0][0]);
                amazonAddressesArray = amazonAddresses.toArray(new String[0][0]);
            } else {
                infoBox("The \"customers.txt\" file could not be found, have you selected the correct folder?", "Error!");
                System.exit(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void getFileNames() {
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jfc.setDialogTitle("*SELECT FOLDER CONTAINING ORDERS AND CUSTOMERS.TXT FILE*");
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            folderPath = jfc.getSelectedFile().getAbsolutePath();
            System.out.println(folderPath);

            File dir = new File(folderPath);
            Collection<String> files = new ArrayList<>();
            if (dir.isDirectory()) {
                File[] listFiles = dir.listFiles();
                if (listFiles != null) {
                    for (File file : listFiles) {
                        if (file.isFile()) {
                            String fileNameWithType = file.getName();
                            // get all html and txt files apart from customer.txt
                            if (!fileNameWithType.equals("customers.txt")) {
                                String[] parts = fileNameWithType.split("\\.");
                                int numberOfParts = parts.length;
                                if (numberOfParts > 0) {
                                    String extension = parts[numberOfParts - 1];
                                    if (extension.equalsIgnoreCase("html") || extension.equalsIgnoreCase("txt")
                                            || extension.equalsIgnoreCase("csv") || extension.equalsIgnoreCase("htm"))
                                        files.add(fileNameWithType);
                                }
                            }
                        }
                    }
                } else {
                    infoBox("There was a problem reading files - are there any?", null);
                    System.exit(0);
                    fileNames = null;
                }
            }
            fileNames = files.toArray(new String[]{});
        } else {
            infoBox("No folder selected", null);
            System.exit(0);
            fileNames = null;
        }
    }

    private static void orderCustomer() {
        try {
            InputStreamReader inputStreamReader = null;
            FileReader fileReader = null;
            BufferedReader bufferedReader;

            // The orders with htm extension have a different charset.
            if (fileExtension.equals("htm")) {
                inputStreamReader = new InputStreamReader(new FileInputStream(orderFile), StandardCharsets.UTF_16);
                bufferedReader = new BufferedReader(inputStreamReader);
            } else {
                fileReader = new FileReader(orderFile);
                bufferedReader = new BufferedReader(fileReader);
            }

            String line;
            int lineCount = 0;

            find_customer_loop:
            while ((line = bufferedReader.readLine()) != null) {
                lineCount++;
                if (line.contains(AMAZON_ORDER_IDENTIFIER)) {
                    System.out.println("Amazon order");
                    // sets to null to make sure a value isn't carried over from a different order. Is handled elsewhere
                    customerID = null;
                    // sets true as Amazon always will use SPCR
                    useSPCR = true;
//                    customerName = null;
                    orderingSystem = AMAZON_ORDER;
                } else {
                    int count = 0;
                    while (count < customers.length) {
                        if (line.contains(customers[count][0])) {
                            System.out.println("Found customer " + customers[count][1]);
                            customerID = customers[count][1];
//                            customerName = customers[count][2];
                            orderingSystem = customers[count][3];
                            useSPCR = customers[count][4].equals(SPCR_USED);
                            empFifthColumn = customers[count][5].equals(FIFTH_COLUMN);
                            // Emporio orders contain many irrelevant lines before the account number / postcode (apart from the PO number
                            // that is also in the file name), skipping to this line in a later process will speed things up.
                            lineToProcessEmpOrderFrom = lineCount;

                            break find_customer_loop;
                        }
                        count++;
                    }
                }
            }
            if (inputStreamReader!=null) inputStreamReader.close();
            else fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void extractANIOrder() {
        int numberOfProductLinesStated = 0;
        int numberOfProductLinesRead = 0;
        boolean readingOrderLines = false;
        List<String[]> list = new ArrayList<>();
        System.out.println("Extracting ANI order");
        try {
            FileReader fileReader = new FileReader(orderFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                // If this is slow I could make it only check a thing if it hasn't been previously found
                if (readingOrderLines) {
                    if (line.trim().isEmpty()) {
                        // Checks for blank line (which is always found after the table of products
                        readingOrderLines = false;
                        orderLines = list.toArray(new String[0][0]);
                        System.out.println("String of order lines created containing " + orderLines.length + " lines");
                    } else {
                        String[] parts = line.split("\\s+");
                        int numberOfParts = parts.length;
                        if (numberOfParts > 2) {
                            String[] orderLineDetails = new String[2];
                            // Product code should be the first second column (there is a white space first)
                            orderLineDetails[0] = parts[1];
                            // Product quantity should be the last column
                            orderLineDetails[1] = parts[numberOfParts - 1];
                            list.add(orderLineDetails);
                            System.out.println("Product code: " + orderLineDetails[0]);
                            System.out.println("Product quantity: " + orderLineDetails[1]);
                            numberOfProductLinesRead++;
                        } else {
                            System.out.println("Order line doesn't contain enough data");
                        }
                    }
                } else if (line.contains(ANI_TABLE_START_IDENTIFIER)) {
                    readingOrderLines = true;
                    System.out.println("Starting to read product lines");
                } else if (line.contains(ANI_ORDER_DATE_IDENTIFIER)) {
                    String dateUnformatted = line.substring(line.length() - 10);
                    wantedDeliveryDate = convertDate(dateUnformatted);
                    System.out.println("Order Date: " + wantedDeliveryDate);
                } else if (line.contains(ANI_PO_IDENTIFIER) && !line.contains(ANI_PO_IDENTIFIER_EXCLUSION)) {
                    String[] parts = line.split("\\s+");
                    int numberOfParts = parts.length;
                    customerPONumber = parts[numberOfParts - 1];
                    System.out.println("PO Number: " + customerPONumber);
                } else if (line.contains(ANI_LINES_IDENTIFIER)) {
                    String[] parts = line.split("\\s+");
                    int numberOfParts = parts.length;
                    if (parts[numberOfParts - 1].matches("\\d+")) {
                        numberOfProductLinesStated = Integer.parseInt(parts[numberOfParts - 1]);
                        System.out.println("Number of Lines Stated " + numberOfProductLinesStated);
                    }
                }
            }
            fileReader.close();
            if (numberOfProductLinesRead == numberOfProductLinesStated) {
                System.out.println("Number of lines stated match lines read");
                aniOrderLinesReadCorrectly++;
            } else {
                infoBox("For the file " + fileName + " not all lines were read correctly", "Error!");
                aniOrderLinesError++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void extractWFMOrder() {
        int numberOfItemsStated = 0;
        int numberOfItemsRead = 0;
        boolean readingOrderLines = false;
        boolean nextLineDate = false;
        boolean nextLinePONumber = false;
        List<String[]> list = new ArrayList<>();
        System.out.println("Extracting WFM order");
        try {
            FileReader fileReader = new FileReader(orderFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                // If this is slow I could make it only check a thing if it hasn't been previously found
                if (readingOrderLines) {
                    if (line.equals("</tr>") || line.equals("<tr>") || line.equals("<td colspan=\"7\">")) continue;
                    if (line.contains("<hr>")) {
                        // Checks for html for horizontal line (which is always found after the table of products
                        readingOrderLines = false;
                        orderLines = list.toArray(new String[0][0]);
                        System.out.println("String of order lines created containing " + orderLines.length + " lines");
                    } else {
                        String[] parts = line.split(WFM_COLUMN_SEPARATOR);
                        int numberOfParts = parts.length;
                        if (numberOfParts > 3) {
                            String[] orderLineDetails = new String[2];
                            // Product code should be the first second column
                            orderLineDetails[0] = removeHtmlAndSpace(parts[1]);
                            // Product quantity should be the third column but this also contains text
                            orderLineDetails[1] = removeHtmlAndSpace(parts[2].replace(WFM_QTY_TEXT_FOR_REMOVAL, ""));
                            list.add(orderLineDetails);
                            System.out.println("Product code: " + orderLineDetails[0]);
                            System.out.println("Product quantity: " + orderLineDetails[1]);
                            if (orderLineDetails[1].matches("\\d+")) {
                                numberOfItemsRead = numberOfItemsRead + Integer.parseInt(orderLineDetails[1]);
                                System.out.println("Number of items read " + numberOfItemsRead);
                            } else {
                                System.out.println("There has been a problem reading the quantity");
                            }
                        } else {
                            System.out.println("Order line doesn't contain enough data");
                        }
                    }
                } else if (nextLineDate) {
                    wantedDeliveryDate = removeHtmlAndSpace(line);
                    System.out.println("Order Date: " + wantedDeliveryDate);
                    nextLineDate = false;
                } else if (nextLinePONumber) {
                    customerPONumber = removeHtmlAndSpace(line);
                    System.out.println("PO Number: " + customerPONumber);
                    nextLinePONumber = false;
                } else if (line.contains(WFM_TABLE_START_IDENTIFIER)) {
                    readingOrderLines = true;
                    System.out.println("Starting to read product lines");
                } else if (line.contains(WFM_ORDER_DATE_IDENTIFIER)) {
                    nextLineDate = true;
                } else if (line.contains(WFM_PO_IDENTIFIER)) {
                    nextLinePONumber = true;
                } else if (line.contains(WFM_TOTAL_ITEMS_IDENTIFIER)) {
                    String[] parts = line.split(WFM_COLUMN_SEPARATOR);
                    int numberOfParts = parts.length;
                    if (numberOfParts > 3) {
                        String numberOfItemsStatedStr = removeHtmlAndSpace(parts[2]);
                        if (numberOfItemsStatedStr.matches("\\d+")) {
                            numberOfItemsStated = Integer.parseInt(numberOfItemsStatedStr);
                            System.out.println("Number of items stated " + numberOfItemsStated);
                        } else {
                            System.out.println("Error reading number of items stated");
                        }
                    }
                }
            }
            fileReader.close();
            if (numberOfItemsRead == numberOfItemsStated) {
                System.out.println("Number of items stated match items read and totaled");
                wfmOrderLinesReadCorrectly++;
            } else {
                infoBox("For the file " + fileName + " not all lines were read correctly", "Error!");
                wfmOrderLinesError++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void extractNatOrder() {
        double valueOfItemsStated = 0;
        double valueOfItemsRead = 0;
        boolean readingOrderLines = false;
        boolean nextLineDate = false;
        boolean nextLinePONumber = false;
        boolean nextLineTotals = false;
        boolean lookingForQuantityColumn = false;
        boolean lookingForPartCodeColumn = false;
        boolean lookingForTotalCostColumn = false;
        String linePartCode = "";


        List<String[]> list = new ArrayList<>();
        System.out.println("Extracting Naturisimo order");
        try {
            FileReader fileReader = new FileReader(orderFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                // If this is slow I could make it only check a thing if it hasn't been previously found
                if (readingOrderLines) {
                    String lineTrimmed = line.trim();
                    if (line.contains(NAT_TOTAL_ITEMS_IDENTIFIER)) {
                        // Checks for totals which is found after the table of products
                        readingOrderLines = false;
                        nextLineTotals = true;
                        orderLines = list.toArray(new String[0][0]);
                        System.out.println("String of order lines created containing " + orderLines.length + " lines");
                    } else if (lineTrimmed.equals("<tr>")) {
                        lookingForPartCodeColumn = true;
                        linePartCode = null;
                    } else if (lookingForPartCodeColumn) {
                        // The part code is in the first column and is the first line after <tr>
                        lookingForPartCodeColumn = false;
                        lookingForQuantityColumn = true;
                        linePartCode = removeHtmlAndSpace(line);
                        System.out.println("Product code: " + linePartCode);
                    } else if (lookingForQuantityColumn) {
                        // Quantity row only contains a number so we keep going through lines until we find this row
                        if (lineTrimmed.matches("\\d+")) {
                            lookingForQuantityColumn = false;
                            lookingForTotalCostColumn = true;
                            // Both quantity and code now read so add to list
                            String[] lineDetails = new String[]{linePartCode, lineTrimmed};
                            list.add(lineDetails);
                            System.out.println("Product quantity: " + lineTrimmed);
                        }
                    } else if (lookingForTotalCostColumn) {
                        // total cost column only contains a number so we keep going through lines until we find this row
                        if (lineTrimmed.matches("\\d+") || lineTrimmed.matches("\\d+\\.\\d+")) {
                            lookingForTotalCostColumn = false;
                            valueOfItemsRead = valueOfItemsRead + Double.parseDouble(lineTrimmed);
                            System.out.println("Total line cost: " + lineTrimmed);
                            System.out.println("Running total: " + valueOfItemsRead);
                        }
                    }
                } else if (nextLineDate) {
                    String dateUnformatted = removeHtmlAndSpace(line);
                    wantedDeliveryDate = convertDate(dateUnformatted);
                    System.out.println("Order Date: " + wantedDeliveryDate);
                    nextLineDate = false;
                } else if (nextLinePONumber) {
                    customerPONumber = removeHtmlAndSpace(line);
                    System.out.println("PO Number: " + customerPONumber);
                    nextLinePONumber = false;
                } else if(nextLineTotals) {
                    String valueOfItemsStatedStr = removeHtmlAndSpace(line).replaceAll(",","");
                    if (valueOfItemsStatedStr.matches("\\d+\\.\\d+")) {
                        valueOfItemsStated = Double.parseDouble(valueOfItemsStatedStr);
                        System.out.println("Total value stated " + valueOfItemsStated);
                        break;
                    } else {
                        System.out.println("Error reading total value");
                    }
                } else if (line.contains(NAT_TABLE_START_IDENTIFIER)) {
                    readingOrderLines = true;
                    System.out.println("Starting to read product lines");
                } else if (line.contains(NAT_ORDER_DATE_IDENTIFIER)) {
                    nextLineDate = true;
                } else if (line.contains(NAT_PO_IDENTIFIER)) {
                    nextLinePONumber = true;
                }
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        double difference = valueOfItemsRead - valueOfItemsStated;
        System.out.println("Total difference: " + difference);
        if (difference < 0.1 && difference > -0.1) {
            System.out.println("Total value of order line match total values given");
            natOrderLinesReadCorrectly++;
        } else {
            infoBox("For the file " + fileName + " the calculated total of the line costs and" +
                    " the total cost given on the order don't match", "Error!");
            natOrderLinesError++;
        }

    }

    private static void extractEmpOrder() {
        System.out.println("Extracting EMP order");
        double calculatedValueOrOrderLines = 0;
        double givenValueOfOrderLines = 0;
        int lineCount = 0;
        boolean readingOrderLines = false;
        boolean lookingForQuantityColumn = false;
        boolean lookingForPartCodeColumn = false;
        boolean lookingForCostColumn = false;
        boolean lookingForTotalCostColumn = false;
        boolean lookingForBarcodeColumn = false;
        boolean lookingForTotal = false;
        List<String[]> list = new ArrayList<>();
        String lineQuantity = null;
        String linePartCode;

        // for Emporio orders the PO number is found in the filename (as well as in the file) and it is quicker to use this
        String[] fileNameParts = fileName.split("#");
        if (fileNameParts.length > 1) customerPONumber = fileNameParts[1];
        else customerPONumber = fileName;
        System.out.println("PO Number: " + customerPONumber);

        // for speed and because the wanted delivery date is not so relevant use today's date as wanted delivery date
        wantedDeliveryDate = todaysDate();
        System.out.println("Today's date used as order date: " + wantedDeliveryDate);

        try {
            InputStreamReader fileReader = new InputStreamReader(new FileInputStream(orderFile), StandardCharsets.UTF_16);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                lineCount++;

                // the below will make it skip to the line where the customer number / postcode was found
                if (lineCount < lineToProcessEmpOrderFrom) continue;

                if (readingOrderLines) {
                    if (lookingForQuantityColumn) {
                        // Quantity column is the first thing looked for in a table row, however the Total found at the end of the
                        // table is also in the first column so check for that first.
                        if (line.contains(EMP_ORDERLINES_END_IDENTIFIER)) {
                            System.out.println("End of product lines");
                            // When the line contains the Total the order lines section has finished
                            readingOrderLines = false;
                            lookingForTotal = true;
                            orderLines = list.toArray(new String[0][0]);
                            System.out.println("String of order lines created containing " + orderLines.length + " lines");
//                            int c = 0;
//                            while (c < orderLines.length) {
//                                System.out.println(orderLines[c][0] + " " + orderLines[c][1]);
//                                c++;
//                            }
                        } else {
                            if (line.contains("Roman\"'>")) {
                                lookingForQuantityColumn = false;
                                lookingForPartCodeColumn = true;
                                String[] parts = line.split("Roman\"'>");
                                int numberOfParts = parts.length;
                                if (numberOfParts > 1) {
                                    String[] partsTwo = parts[1].split("<");
                                    if (partsTwo.length > 1) {
                                        // Add product quantity to array
                                        lineQuantity = partsTwo[0];
                                        System.out.println("Product quantity: " + lineQuantity);
                                    } else {
                                        System.out.println("Error reading quantity");
                                    }
                                } else {
                                    System.out.println("Error reading quantity");
                                }
                            }
                        }
                    } else if (lookingForPartCodeColumn) {
                        // The part code is in the second column
                        if (line.contains("Roman\"'>")) {
                            lookingForPartCodeColumn = false;
                            lookingForCostColumn = true;

                            // if the customer doesn't use the partcode column properly but does use the barcode column we'll use that instead
                            if (!empFifthColumn) {
                                String[] parts = line.split("Roman\"'>");
                                int numberOfParts = parts.length;
                                if (numberOfParts > 1) {
                                    String[] partsTwo = parts[1].split("<");
                                    if (partsTwo.length > 1) {
                                        // Add product code to array
                                        linePartCode = partsTwo[0];
                                        // Both quantity and code now read so add to list
                                        String[] lineDetails = new String[]{linePartCode, lineQuantity};
                                        list.add(lineDetails);
                                        System.out.println("Product code: " + linePartCode);
                                    } else {
                                        System.out.println("Error reading product code");
                                    }
                                } else {
                                    System.out.println("Error reading product code");
                                }
                            }
                        }
                    } else if (lookingForCostColumn) {
                        // cost column is the third column and not used
                        if (line.contains("Roman\"'>")) {
                            lookingForCostColumn = false;
                            lookingForTotalCostColumn = true;
                        }
                    } else if (lookingForTotalCostColumn) {
                        // total cost column is the fourth column and is used to check all lines read correctly
                        if (line.contains("Roman\"'>")) {
                            lookingForTotalCostColumn = false;
                            lookingForBarcodeColumn = true;
                            String[] parts = line.split("Roman\"'>");
                            int numberOfParts = parts.length;
                            if (numberOfParts > 1) {
                                String[] partsTwo = parts[1].split("<");
                                if (partsTwo.length > 1) {
                                    String value = partsTwo[0].replace(",", "");
                                    if (value.matches("\\d+\\.\\d+")) {
                                        calculatedValueOrOrderLines = calculatedValueOrOrderLines + Double.parseDouble(value);
                                        System.out.println("Total line cost: " + value);
                                        System.out.println("Running total: " + calculatedValueOrOrderLines);
                                    } else {
                                        System.out.println("Error reading total line cost");
                                    }
                                } else {
                                    System.out.println("Error reading total line cost");
                                }
                            } else {
                                System.out.println("Error reading total line cost");
                            }
                        }
                    } else if(lookingForBarcodeColumn) {
                        // barcode column is the fifth column and is used where partcode column is no good. It is the last column we are interested in
                        if (line.contains("Roman\"'>")) {
                            lookingForBarcodeColumn = false;
                            // if the customer doesn't use the partcode column properly but does use the barcode column we'll use that instead
                            if (empFifthColumn) {
                                String[] parts = line.split("Roman\"'>");
                                int numberOfParts = parts.length;
                                if (numberOfParts > 1) {
                                    String[] partsTwo = parts[1].split("<");
                                    if (partsTwo.length > 1) {
                                        // Add product code to array
                                        linePartCode = partsTwo[0];
                                        // Both quantity and code now read so add to list
                                        String[] lineDetails = new String[]{linePartCode, lineQuantity};
                                        list.add(lineDetails);
                                        System.out.println("Product code: " + linePartCode);
                                    } else {
                                        System.out.println("Error reading barcode");
                                    }
                                } else {
                                    System.out.println("Error reading barcode");
                                }
                            }
                        }
                    } else if (line.contains("<tr ")) {
                        lookingForQuantityColumn = true;
                        // clear previous details for starting new line
//                        orderLineDetails[0] = orderLineDetails[1] = null;
                        lineQuantity = null;
                    }
                } else if (lookingForTotal) {
                    // check total order value calculated against total order value stated
                    if (line.contains("£") || line.contains("$")) {
                        String[] parts = line.split("[£$]");
                        int numberOfParts = parts.length;
                        if (numberOfParts > 1) {
                            String[] partsTwo = parts[1].split("<");
                            if (partsTwo.length > 1) {
                                String value = partsTwo[0].replace(",", "");
                                if (value.matches("\\d+\\.\\d+")) {
                                    givenValueOfOrderLines = Double.parseDouble(value);
                                    System.out.println("Total value stated " + givenValueOfOrderLines);
                                } else {
                                    System.out.println("Error reading total value");
                                }
                            } else {
                                System.out.println("Error reading total value");
                            }
                        } else {
                            System.out.println("Error reading total value");
                        }
                        // all data required has been extracted
                        break;
                    }
                } else if (line.contains(EMP_ORDERLINES_START_IDENTIFIER)) {
                    readingOrderLines = true;
                    System.out.println("Starting to read product lines");
                }
            }
            fileReader.close();
            double difference = calculatedValueOrOrderLines - givenValueOfOrderLines;
            System.out.println("Total difference: " + difference);
            if (difference < 0.1 && difference > -0.1) {
                System.out.println("Total value of order line match total values given");
                empOrderLinesReadCorrectly++;
            } else {
                infoBox("For the file " + fileName + " the calculated total of the line costs and" +
                        " the total cost given on the order don't match", "Error!");
                empOrderLinesError++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processAmazonOrder() {
        List<String[]> list = new ArrayList<>();
        System.out.println("Extracting Amazon orders");
        try {
            FileReader fileReader = new FileReader(orderFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            String[] parts;
            freshOrPantry = false;
            amazonSups = false;
            int numberOfLinesRead = 0;
            int numberOfItems = 0;
            while ((line = bufferedReader.readLine()) != null) {
                // Skip the first line
                if (numberOfLinesRead == 0) {
                    numberOfLinesRead++;
                    continue;
                }
                parts = splitCSVWithQuotes(line);
                if (parts != null && (parts.length > 0)) {
                    // Amazon order files should have 19 columns - though the last one is blank!
//                    System.out.println("parts.length = " + parts.length);
                    if (parts.length == 20) {
                        // Some lines on an order might have been cancelled (qty confirmed as 0), this checks for that
                        if (parts[14].equals("0")) {
                            System.out.println("One line skipped as marked quantity = 0");
                            numberOfLinesRead++;
                            continue;
                        }
                        String currentLinePO = parts[0];
                        if (customerPONumber == null) {
                            // Handle the first order
                            customerPONumber = currentLinePO;
                            getAmazonHeaderDetails(parts);
                            // if all has gone well customerID should now be populated
                            if (customerID == null) {
                                infoBox("Amazon account " + parts[1] + " not found", "Error");
                                continue;
                            }
                            String[] orderLineDetails = getAmazonLineDetails(parts);
                            list.add(orderLineDetails);
                            numberOfItems = numberOfItems + Integer.parseInt(orderLineDetails[1]);
                        } else if (currentLinePO.equals(customerPONumber)) {
                            // Handle a continuation of the same order
                            String[] orderLineDetails = getAmazonLineDetails(parts);
                            list.add(orderLineDetails);
                            numberOfItems = numberOfItems + Integer.parseInt(orderLineDetails[1]);
                        } else {
                            // this marks the start of a new order so the old order needs completing and the new starting.
                            orderLines = list.toArray(new String[0][0]);
                            amazonOrdersExtracted++;
                            // if the order is not sups and not pantry or fresh check if more than 99 cases of items(tea) on the order
                            if (freshOrPantry || amazonSups) treatAsItemsMoreThan99 = false;
                            else treatAsItemsMoreThan99 = numberOfItems > 99;
                            createXML();
                            clearFieldsForNewAmazonOrder();
                            numberOfItems = 0;
                            list.clear();

                            // Get header details for new order
                            customerPONumber = currentLinePO;
                            getAmazonHeaderDetails(parts);
                            // if all has gone well customerID should now be populated
                            if (customerID == null) {
                                infoBox("Amazon account " + parts[1] + " not found", "Error");
                                continue;
                            }
                            String[] orderLineDetails = getAmazonLineDetails(parts);
                            list.add(orderLineDetails);
                            numberOfItems = numberOfItems + Integer.parseInt(orderLineDetails[1]);
                        }
                    } else {
                        infoBox("For Amazon order " + parts[0] +
                                " one line contained errors and was omitted", null);
                    }
                } else {
                    infoBox("A line on an Amazon order could not be read", null);
                }
                numberOfLinesRead++;
            }
            // Handle the final order
            if (!list.isEmpty()) {
                orderLines = list.toArray(new String[0][0]);
                amazonOrdersExtracted++;
                // if the order is not sups and not pantry or fresh check if more than 99 cases of items(tea) on the order
                if (freshOrPantry || amazonSups) treatAsItemsMoreThan99 = false;
                else treatAsItemsMoreThan99 = numberOfItems > 99;
                createXML();
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void getAmazonHeaderDetails(String[] parts) {
        // Used the 'Deliver to' address
        wantedDeliveryDate = convertDate(parts[9]);
        String addressCodePrefix = null;
        int count = 0;
        while (count < amazonAccountsArray.length) {
            if (parts[1].equals(amazonAccountsArray[count][0])) {
                customerID = amazonAccountsArray[count][1];
                // is this an Amazon Sups order?
                amazonSups = (customerID.equals(AMAZON_SUPS));
//                customerName = amazonAccountsArray[count][2];
                addressCodePrefix = amazonAccountsArray[count][3];
                break;
            }
            count++;
        }
        count = 0;
        while (count < amazonAddressesArray.length) {
            if (parts[2].contains(amazonAddressesArray[count][0])) {
                amzAddressCode = addressCodePrefix + amazonAddressesArray[count][1];
                break;
            }
            count++;
        }
    }

    private static String[] getAmazonLineDetails(String[] parts) {
        String[] orderLineDetails = new String[2];
        // Product code should be the fifth column
        orderLineDetails[0] = parts[4];
        // Product quantity should be the last column, but for Grocery or Pantry this will need multiplying
        if ((parts[1].equals(AMAZON_FRESH)) | (parts[1].equals(AMAZON_PANTRY))) {
            freshOrPantry = true;
            // parts[13] is the Submitted Cases column. parts[14] is the Accepted cases column which could be used instead
            // Steve asked to change this so quantities remain as case quantities - he will update SPCR
            orderLineDetails[1] = parts[13];
//            int initalQuantity = Integer.parseInt(parts[13]);
//            orderLineDetails[1] = Integer.toString(initalQuantity * 4);
        } else {
            orderLineDetails[1] = parts[14];
            freshOrPantry = false;
        }
        return orderLineDetails;
    }

    private static String[] splitCSVWithQuotes(String line) {
        if (line != null) {
            String[] parts = line.split(",", -1);
            int noOfParts = parts.length;
            int count = 0;
            boolean isInQuotationMarks = false;
            StringBuilder sb = new StringBuilder();
            ArrayList<String> list = new ArrayList<>();
            while (count < noOfParts) {
                if (!isInQuotationMarks) {
                    if (parts[count].startsWith("\"")) {
                        if (parts[count].endsWith("\"")) {
                            // the quote both starts and ends with " so can add it as it is
                            list.add(parts[count]);
                            isInQuotationMarks = false;
                        } else {
                            // it is the start of a quote but a comma splits it so it rebuilds the string adding back the comma
                            isInQuotationMarks = true;
                            sb = new StringBuilder();
                            sb.append(parts[count]).append(",");
                        }
                    } else {
                        // no quote has been flagged as started and this part isn't the start of a quote so is added as is
                        list.add(parts[count]);
                    }
                } else {
                    if (parts[count].endsWith("\"")) {
                        // the quote ends with this part so it is completed and added to the list
                        sb.append(parts[count]);
                        list.add(sb.toString());
                        isInQuotationMarks = false;
                    } else {
                        // the quote is ongoing so this section is added with the comma added back in
                        // it also checks that it hasn't got the the end of the line without the quote closing and if
                        // it has it handles this
                        if (count == (noOfParts - 1)) {
                            sb.append(parts[count]);
                            list.add(sb.toString());
                        } else sb.append(parts[count]).append(",");
                    }
                }
                count++;
            }
            return list.toArray(new String[0]);
        } else {
            System.out.println("String line is null");
            return null;
        }
    }

    private static void createXML() {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // root element
            Element root = document.createElement("ORDERS");
            document.appendChild(root);

            // set an attribute to root element
            Attr attr = document.createAttribute("xmlns");
            attr.setValue("urn:ifsworld-com:schemas:receive_customer_order");
            root.setAttributeNode(attr);

            Element contract = document.createElement("CONTRACT");
            if (orderingSystem.equals(AMAZON_ORDER)) {
                if (treatAsItemsMoreThan99) contract.appendChild(document.createTextNode("04"));
                else contract.appendChild(document.createTextNode("03"));
            } else {
                contract.appendChild(document.createTextNode("03"));
            }
            root.appendChild(contract);

            Element currencyCode = document.createElement("CURRENCY_CODE");
            currencyCode.appendChild(document.createTextNode("GBP"));
            root.appendChild(currencyCode);

            Element customerPONo = document.createElement("CUSTOMER_PO_NO");
            customerPONo.appendChild(document.createTextNode(customerPONumber));
            root.appendChild(customerPONo);

            Element ourCustomerNo = document.createElement("OUR_CUSTOMER_NO");
            ourCustomerNo.appendChild(document.createTextNode(customerID));
            root.appendChild(ourCustomerNo);

            // These address IDs have to be unique and would be something the customers would provide,
            // however in the absence of that these will be populated both here and in IFS with the IFS customer ID
            Element eanLocationDeliveryAddress = document.createElement("EAN_LOCATION_DELIVERY_ADDRESS");
            if (orderingSystem.equals(AMAZON_ORDER)) {
                eanLocationDeliveryAddress.appendChild(document.createTextNode(amzAddressCode));
            } else {
                eanLocationDeliveryAddress.appendChild(document.createTextNode(customerID));
            }
            root.appendChild(eanLocationDeliveryAddress);

            // After testing it appears the below two aren't needed - IFS uses the default on the accounts
            Element eanLocationDocumentAddress = document.createElement("EAN_LOCATION_DOCUMENT_ADDRESS");
            root.appendChild(eanLocationDocumentAddress);

            Element eanSupplierDocumentAddress = document.createElement("EAN_SUPPLIER_DOCUMENT_ADDRESS");
            root.appendChild(eanSupplierDocumentAddress);

            Element receiverCommunicationID = document.createElement("RECEIVER_COMMUNICATION_ID");
            receiverCommunicationID.appendChild(document.createTextNode("CONNECT"));
            root.appendChild(receiverCommunicationID);

            Element senderCommunicationID = document.createElement("SENDER_COMMUNICATION_ID");
            senderCommunicationID.appendChild(document.createTextNode("EXTCOMP701"));
            root.appendChild(senderCommunicationID);

            // wantedDeliveryDate needs format 2018-06-24T09:00:00
            Element wantedDeliveryDateElement = document.createElement("WANTED_DELIVERY_DATE");
            wantedDeliveryDateElement.appendChild(document.createTextNode(wantedDeliveryDate + "T09:00:00"));
            root.appendChild(wantedDeliveryDateElement);

            Element headerNote = document.createElement("HEADER_NOTE");
            if (orderingSystem.equals(AMAZON_ORDER)) {
                if (treatAsItemsMoreThan99) headerNote.appendChild(document.createTextNode("*****100 or more items/cases*****"));
                else headerNote.appendChild(document.createTextNode(""));
            } else {
                headerNote.appendChild(document.createTextNode("OrderAuto_OrderConverter"));
            }
            root.appendChild(headerNote);

            Element labelNote = document.createElement("LABEL_NOTE");
            // labelNote.appendChild(document.createTextNode(""));
            root.appendChild(labelNote);

            Element orderLinesElement = document.createElement("ORDER_LINES");
            root.appendChild(orderLinesElement);

            int orderLinesLength = orderLines.length;
            int line = 0;
            while (line < orderLinesLength) {
                Element orderLineElement = document.createElement("ORDER_LINE");
                orderLinesElement.appendChild(orderLineElement);

                Element orderLineNumber = document.createElement("ORDER_LINE_NO");
                orderLineNumber.appendChild(document.createTextNode(Integer.toString(line + 1)));
                orderLineElement.appendChild(orderLineNumber);

                Element releaseNo = document.createElement("RELEASE_NO");
                releaseNo.appendChild(document.createTextNode("1"));
                orderLineElement.appendChild(releaseNo);

                // TODO IFS is wanting more info for some fields - check what and why
                // If customer uses their own codes or wrong quantities
                Element partNo = document.createElement("PART_NO");
                if (useSPCR) partNo.appendChild(document.createTextNode(orderLines[line][0]));
//                partNo.appendChild(document.createTextNode(orderLines[line][0]));
                orderLineElement.appendChild(partNo);

                Element gtinNo = document.createElement("GTIN_NO");
                // gtinNo.appendChild(document.createTextNode(""));
                orderLineElement.appendChild(gtinNo);

                // If customer uses our Sales Part codes
                Element vendorPartNo = document.createElement("VENDOR_PART_NO");
                if (!useSPCR) vendorPartNo.appendChild(document.createTextNode(orderLines[line][0]));
                orderLineElement.appendChild(vendorPartNo);

                Element buyQuantityDue = document.createElement("BUY_QUANTITY_DUE");
                // buyQuantityDue.appendChild(document.createTextNode(""));
                orderLineElement.appendChild(buyQuantityDue);

                // If customer uses their own codes or wrong quantities
                Element customerBuyQuantity = document.createElement("CUSTOMER_BUY_QUANTITY");
                if (useSPCR) customerBuyQuantity.appendChild(document.createTextNode(orderLines[line][1]));
//                customerBuyQuantity.appendChild(document.createTextNode(orderLines[line][1]));
                orderLineElement.appendChild(customerBuyQuantity);

                // If customer uses our codes
                Element inputQty = document.createElement("INPUT_QTY");
                if (!useSPCR) inputQty.appendChild(document.createTextNode(orderLines[line][1]));
                orderLineElement.appendChild(inputQty);

                // wantedDeliveryDate needs format 2018-06-24T09:00:00
                Element wantedDeliveryDateLineElement = document.createElement("WANTED_DELIVERY_DATE");
                wantedDeliveryDateLineElement.appendChild(document.createTextNode(wantedDeliveryDate + "T09:00:00"));
                orderLineElement.appendChild(wantedDeliveryDateLineElement);

                line++;
            }

            // create the xml file
            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult;
            if (orderingSystem.equals(AMAZON_ORDER)) {
                streamResult = new StreamResult(new File(folderPath + "\\" + customerPONumber + ".xml"));
//                System.out.println("XML saved: " + folderPath + "\\" + customerPONumber + ".xml");
            } else {
                String fileNameEdited = fileName.replaceAll("#","");
                streamResult = new StreamResult(new File(folderPath + "\\" + fileNameEdited + ".xml"));
//                System.out.println("XML saved: " + folderPath + "\\" + fileName + ".xml");
            }
            transformer.transform(domSource, streamResult);
            xmlFilesCreated++;

        } catch (
                ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
    }

    private static String removeHtmlAndSpace(String str) {
        String htmlGone = str.replaceAll("<.*?>", "");
        return htmlGone.trim();
    }

    private static String convertDate(String dateUnformatted) {
        // from DD/MM/YYYY to YYYY-MM-DD
        String[] dateParts = dateUnformatted.split("/");
        if (dateParts.length == 3) {
            String year = dateParts[2];
            String month = dateParts[1].trim();
            if (month.length() == 1) month = "0" + month;
            String day = dateParts[0].trim();
            if (day.length() == 1) day = "0" + day;
            return year + "-" + month + "-" + day;
        } else {
            System.out.println("There has been a problem with the date");
            return null;
        }
    }

    private static String todaysDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private static void clearFields() {
        orderFile = null;
        fileName = null;
        customerID = null;
//        customerName = null;
        orderingSystem = null;
        customerPONumber = null;
        wantedDeliveryDate = null;
        orderLines = null;
    }

    private static void clearFieldsForNewAmazonOrder() {
        customerID = null;
//        customerName = null;
        customerPONumber = null;
        wantedDeliveryDate = null;
        orderLines = null;
        amzAddressCode = null;
    }

    private static void infoBox(String infoMessage, String titleBar) {
        if (titleBar == null) titleBar = "Order Converter";
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
}
